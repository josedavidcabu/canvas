

    var c = document.getElementById("miCanvas");
    var ctx = c.getContext("2d");
    //Escenario 
    ctx.beginPath();
    ctx.fillStyle = "#1EC4E5";
    ctx.fillRect(0, 0, 1000, 450);
    ctx.fill();
    
    
    
    //Seteo los bordes  y ajusto todo par circuferencia
    ctx.lineWidth = 5;
    ctx.strokeStyle = "#212121";
    ctx.fillStyle = "#F5EB23";
    ctx.beginPath();
    ctx.arc(750, 70, 50, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();

    //cuerpo de la casa
    ctx.beginPath();
    ctx.fillStyle = "#5D2595";
    ctx.fillRect(320, 249, 400, 200);
    ctx.fill();
    //bordes del cuerpo de la casa
    ctx.beginPath();
    ctx.lineTo(320, 250);
    ctx.lineTo(320, 450);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(720, 250);
    ctx.lineTo(720, 450);
    ctx.stroke();
    //coloreo techo arriba
    ctx.fillStyle = "#755321";
    ctx.beginPath();
    ctx.moveTo(521, 80);
    ctx.lineTo(289, 250);
    ctx.lineTo(751, 250);
    ctx.fill();


    //coloreo la parte de arriba
    ctx.fillStyle = "#5D2595";
    ctx.beginPath();
    ctx.moveTo(521, 100);
    ctx.lineTo(720, 250);
    ctx.lineTo(319, 250);
    ctx.fill();

    //bordes del techo
    ctx.beginPath();
    ctx.lineTo(721, 250);
    ctx.lineTo(519, 100);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(319, 250);
    ctx.lineTo(521, 100);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(319, 250);
    ctx.lineTo(289, 250);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(289, 250);
    ctx.lineTo(521, 80);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(721, 250);
    ctx.lineTo(751, 250);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(751, 250);
    ctx.lineTo(521, 80);
    ctx.stroke();

    //hago la linea horizontal q abarca toda la imagen
    ctx.beginPath();
    ctx.lineTo(1000, 450);
    ctx.lineTo(0, 450);
    ctx.closePath();
    ctx.stroke();


    //color borde de la puerta
    ctx.beginPath();
    ctx.fillStyle = "#E9A90D";
    ctx.fillRect(460, 325, 115, 125);
    ctx.fill();

    //margen de la puerta
    ctx.beginPath();
    ctx.strokeRect(480, 320, 75, 125);
    ctx.stroke();

    //decoracion arriba de la puerta
    ctx.fillStyle = "#1DA0AC";
    ctx.beginPath();
    ctx.moveTo(520, 290);
    ctx.lineTo(420, 320);
    ctx.lineTo(630, 320);
    ctx.fill();

    ctx.beginPath();
    ctx.lineTo(521, 287);
    ctx.lineTo(415, 322);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(520, 287);
    ctx.lineTo(630, 321);
    ctx.stroke();


    //color de la puerta
    ctx.beginPath();
    ctx.fillStyle = "#755321";
    ctx.fillRect(482, 321, 71, 122);
    ctx.fill();

    ctx.beginPath();
    ctx.lineTo(415, 322);
    ctx.lineTo(632, 322);
    ctx.stroke();
    //ventanas
    ctx.fillStyle = "#296AC9";
    ctx.beginPath();
    ctx.fillRect(395, 340, 50, 70);
    ctx.fill();

    ctx.beginPath();
    ctx.strokeRect(395, 340, 50, 70);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(420, 340);
    ctx.lineTo(420, 410);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(395, 375);
    ctx.lineTo(445, 375);
    ctx.stroke();

    ctx.fillStyle = "#296AC9";
    ctx.beginPath();
    ctx.fillRect(590, 340, 50, 70);
    ctx.fill();

    ctx.beginPath();
    ctx.strokeRect(590, 340, 50, 70);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(615, 340);
    ctx.lineTo(615, 410);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(590, 375);
    ctx.lineTo(640, 375);
    ctx.stroke();
    //perilla de la puerta
    ctx.fillStyle = "#F5EB23";
    ctx.beginPath();
    ctx.arc(495, 375, 10, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
    //borde de la puerta
    ctx.beginPath();
    ctx.strokeRect(460, 325, 115, 125);
    ctx.stroke();
    //arboles
    ctx.strokeStyle = "#685109";
    ctx.beginPath();
    ctx.lineTo(100, 300);
    ctx.lineTo(100, 450);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineTo(800,300);
    ctx.lineTo(800,450);
    ctx.stroke();
    //ramas 
    ctx.strokeStyle = "#096816";
    ctx.beginPath();
    ctx.arc(80, 300, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);

    ctx.stroke();
    ctx.beginPath();
    ctx.arc(80, 320, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(80, 340, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(80, 340, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(80, 360, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(80, 380, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(120, 300, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(120, 320, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(120, 340, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(120, 360, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(120, 380, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();

    

    ctx.beginPath();
    ctx.arc(780, 300, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(820, 320, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(780, 320, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(820, 340, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(780, 340, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(820, 360, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(780, 360, 20, (80 / 180) * Math.PI, (10 / 180) * Math.PI, true);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(820, 300, 20, (100 / 180) * Math.PI,  0.90* Math.PI, false);
    ctx.stroke();
